import inspect
import types

from decorator import decorator

from .check_context import CheckContext
from .exceptions import CheckDefinitionError


def argcheck(*args, **kwargs):
    return argcheck_decorate(*args, **kwargs)


def value(argument_name: str) -> object:
    """
    Gets the value of the argument based on the specified argument name in the current check context.

    :param argument_name: str
        Name of the argument to get value
    :return:
        The value of the argument
    """
    for i in range(len(inspect.stack())):
        caller_object = inspect.stack()[i][0].f_locals.get('self')
        if isinstance(caller_object, CheckContext):
            return caller_object.get_argument_value(argument_name)


def argcheck_decorate(*args, **kwargs) -> callable:
    if args:
        if isinstance(args[0], types.FunctionType):
            func = args[0]
            return _argcheck_decorate(func, **kwargs)
        else:
            raise CheckDefinitionError('@argcheck only supports keyword arguments')
    else:
        def argcheck_decorate_wrapper(f):
            try:
                return _argcheck_decorate(f, **kwargs)
            except CheckDefinitionError as e:
                raise e.copy()

        return argcheck_decorate_wrapper


def _argcheck_decorate(func: callable, **checks) -> callable:
    context = CheckContext(inspect.signature(func), checks)

    def _checker(wrapped_func, *args, **kwargs):
        context.bind(*args, **kwargs)
        context.check_all()
        return wrapped_func(*args, **kwargs)

    wrapper = decorator(_checker, func)
    return wrapper
