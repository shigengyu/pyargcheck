import enum
from typing import Tuple, List


class CheckMessageLevel(enum.Enum):
    INFO = 'Info'
    WARN = 'Warn'
    ERROR = 'Error'


class CheckResult(object):
    __slots__ = {'_messages'}

    def __init__(self) -> List[Tuple[CheckMessageLevel, str]]:
        self._messages = list()

    @staticmethod
    def passed():
        return CheckResult()

    @staticmethod
    def failed(message: str = None):
        return CheckResult().error(message)

    @property
    def all_passed(self) -> bool:
        return CheckMessageLevel.ERROR not in set(map(lambda x: x[0], self._messages))

    @property
    def messages(self):
        return self._messages

    def info(self, message: str):
        self._messages += [(CheckMessageLevel.INFO, message)]
        return self

    def warn(self, message: str):
        self._messages += [(CheckMessageLevel.WARN, message)]
        return self

    def error(self, message: str):
        self._messages += [(CheckMessageLevel.ERROR, message)]
        return self
