import inspect

from .check_results import CheckResult
from .checkers import FunctionChecker, ClassChecker
from .exceptions import CheckDefinitionError
from .logger import get_logger

_logger = get_logger()


class CheckContext(object):
    def __init__(self, signature, checks) -> None:
        self._signature = signature
        self._bound_arguments = None

        self._checkers = list()
        for argument_name, check in checks.items():
            if inspect.isclass(check):
                self._checkers.append(ClassChecker(self, argument_name, check))
            elif inspect.isfunction(check):
                self._checkers.append(FunctionChecker(self, argument_name, check))
            else:
                pass

    def bind(self, *args, **kwargs) -> None:
        self._bound_arguments = self._signature.bind(*args, **kwargs)

    def get_argument_value(self, argument_name) -> object:
        if argument_name not in self._bound_arguments.arguments.keys():
            raise CheckDefinitionError('Argument [{}] does not exist'.format(argument_name))
        return self._bound_arguments.arguments[argument_name]

    def check_all(self):
        for checker in self._checkers:
            try:
                check_result = checker.check()
                if check_result is None:
                    check_result = CheckResult.passed()
                elif isinstance(check_result, bool):
                    check_result = CheckResult.passed() if check_result else CheckResult.failed()
            except ValueError as e:
                check_result = CheckResult.failed(str(e))

            _logger.info('Check Result for [%s] = [%r]' % (checker.argument_name, check_result.all_passed))
            if not check_result.all_passed:
                for level, message in check_result.messages:
                    _logger.info(message)
