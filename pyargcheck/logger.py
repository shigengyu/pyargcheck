import inspect
import logging
import sys
import time


def get_logger(name=None, short_module_names=False):
    if name is None:
        caller = inspect.stack()[1][0]
        module_name = inspect.getmodule(caller).__name__
        calling_class = inspect.getouterframes(caller)[0][3]

        if short_module_names:
            module_name = '.'.join(x[0] if str.isalpha(x[0]) else x for x in module_name.split('.'))

        logger_name = module_name
        if calling_class != '<module>':
            logger_name += '.' + calling_class
    else:
        logger_name = name

    return logging.getLogger(logger_name)


class UTCFormatter(logging.Formatter):
    converter = time.gmtime


def init_logging(level=logging.INFO,
                 process_ids=False,
                 process_names=False,
                 thread_names=False,
                 line_numbers=False):
    log_format = '%(asctime)s - {p}{pid}{t}%(name)s - {ln}%(levelname)s - %(message)s'.format(pid='%(process)s - ' if process_ids else '',
                                                                                              p='%(processName)s - ' if process_names else '',
                                                                                              t='%(threadName)s - ' if thread_names else '',
                                                                                              ln='%(lineno)d - ' if line_numbers else '')

    logging.captureWarnings(True)

    # Get root logger
    root_logger = logging.getLogger()

    # Remove all existing handlers
    for handler in root_logger.handlers:
        root_logger.removeHandler(handler)

    # Setup root logger level
    root_logger.setLevel(level)

    # Add console handler
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(UTCFormatter(log_format))
    root_logger.addHandler(console_handler)
