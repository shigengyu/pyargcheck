from abc import ABCMeta, abstractmethod

from .check_results import CheckResult
from .utils import get_type_display_name


class CheckerBase(metaclass=ABCMeta):
    def __init__(self, context, argument_name) -> None:
        self._context = context
        self._argument_name = argument_name

    @property
    def argument_name(self) -> str:
        return self._argument_name

    @abstractmethod
    def check(self):
        raise NotImplementedError()


class ClassChecker(CheckerBase):
    def __init__(self, context, argument_name, expected_type) -> None:
        super().__init__(context, argument_name)
        self._expected_type = expected_type

    def check(self):
        argument_value = self._context.get_argument_value(self.argument_name)
        if not isinstance(argument_value, self._expected_type):
            return CheckResult().error('Argument [{argument_name}] expected type [{expected_type}], but was [{actual_type}]'.format(
                argument_name=self.argument_name,
                expected_type=get_type_display_name(self._expected_type),
                actual_type=get_type_display_name(type(argument_value))
            ))
        else:
            return CheckResult.passed()


class FunctionChecker(CheckerBase):
    def __init__(self, context, argument_name, checker_func) -> None:
        super().__init__(context, argument_name)
        self._checker_func = checker_func

    def check(self):
        argument_value = self._context.get_argument_value(self._argument_name)
        result = self._checker_func(argument_value)
        return result


class ConfigurableChecker(CheckerBase):
    def __init__(self, context, argument_name) -> None:
        super().__init__(context, argument_name)

    def check(self):
        pass
