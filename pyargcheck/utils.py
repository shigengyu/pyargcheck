import builtins
from abc import ABCMeta


def get_type_display_name(t):
    """
    Get the full display name of a type.

    :param t: type
        The specified type
    :return: str:
        The display name of the type

    >>> get_type_display_name(int)
    'int'
    >>> get_type_display_name(float)
    'float'
    >>> get_type_display_name(ABCMeta)
    'abc.ABCMeta'
    """
    m = t.__module__
    name = t.__name__

    if m == builtins.__name__:
        return name
    else:
        return m + '.' + name
