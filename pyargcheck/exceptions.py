class CheckDefinitionError(Exception):
    def copy(self):
        return type(self)(*self.args)
