from pyargcheck import argcheck, value
from pyargcheck.logger import init_logging


@argcheck(first=lambda x: x < value('second'), second=float, third=lambda x: x < value('second'))
def target_func(first, second, *args, third=None):
    pass


@argcheck(first=int, second=int)
def add(first, second):
    return first + second


if __name__ == '__main__':
    init_logging()
    print(add(1, 2))
