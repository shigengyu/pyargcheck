import unittest

from pyargcheck.check_results import CheckResult, CheckMessageLevel


class TestCheckResult(unittest.TestCase):
    def test_check_result_empty(self):
        self.assertEqual(len(CheckResult().messages), 0)

    def test_check_result_info(self):
        check_result = CheckResult().info('Info')
        self.assertEqual(len(check_result.messages), 1)
        self.assertEqual(check_result.messages[0], (CheckMessageLevel.INFO, 'Info'))
