import unittest

import mock
from mock import patch

from pyargcheck import argcheck
from pyargcheck.check_context import CheckContext
from pyargcheck.check_results import CheckResult
from pyargcheck.checkers import FunctionChecker, ClassChecker


@argcheck(first=int, second=int)
def add_checked_once(first, second):
    return first + second


@argcheck(first=int, second=int)
@argcheck(first=int, second=int)
def add_checked_twice(first, second):
    return first + second


class TestSingleArgCheck(unittest.TestCase):
    def test_check_context_check_all_called_once(self):
        with patch.object(CheckContext, 'check_all') as check_all:
            self.assertEqual(add_checked_once(1, 2), 3)
            check_all.assert_called_once_with()

    def test_class_checker_check_called_once(self):
        with patch.object(ClassChecker, 'check', return_value=CheckResult.passed()) as check:
            self.assertEqual(add_checked_twice(1, 2), 3)
            check.assert_has_calls([mock.call()] * 2)

    def test_function_checker_check_not_called(self):
        with patch.object(FunctionChecker, 'check', return_value=CheckResult.passed()) as check:
            self.assertEqual(add_checked_twice(1, 2), 3)
            check.assert_not_called()


class TestMultipleArgCheck(unittest.TestCase):
    def test_check_context_check_all_called_twice(self):
        with patch.object(CheckContext, 'check_all') as check_all:
            self.assertEqual(add_checked_twice(1, 2), 3)
            check_all.assert_has_calls([mock.call()] * 2)

    def test_class_checker_check_called_twice(self):
        with patch.object(ClassChecker, 'check', return_value=CheckResult.passed()) as check:
            self.assertEqual(add_checked_twice(1, 2), 3)
            check.assert_has_calls([mock.call()] * 4)

    def test_function_checker_check_not_called(self):
        with patch.object(FunctionChecker, 'check', return_value=CheckResult.passed()) as check:
            self.assertEqual(add_checked_twice(1, 2), 3)
            check.assert_not_called()
