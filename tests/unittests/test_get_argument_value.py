import unittest

import mock

from pyargcheck import argcheck

value = mock.MagicMock()


@argcheck(first=list, second=lambda x: x in value('first'))
def exclude(first, second):
    return [x for x in first if x != second]


class TestArgCheckValue(unittest.TestCase):
    def test_get_value(self):
        self.assertListEqual(exclude(list(range(3)), 1), [0, 2])
        value.assert_called_once_with('first')
